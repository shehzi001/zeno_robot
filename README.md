# Important usage Instruction
The softwares installation needs to be done on remote workstation. It is not required to be on zeno's computer except some commands/instructions(mentioned in this wiki) which are required to initialize communication between zeno's pc and remote workstation.

# Pre-requisite Installations
## Install Ubuntu

The repository and its related components have been tested under the following Ubuntu distributions:
- ROS Hydro: Ubuntu 12.04

If you do not have a Ubuntu distribution on your computer you can download it from here

     http://www.ubuntu.com/download

## Git - Version Control
### Install Git Software
Install the Git core components and some additional GUI's for the version control

     sudo apt-get install git-core gitg gitk

### Set Up Git
Now it's time to configure your settings. To do this you need to open a new Terminal. First you need to tell git your name, so that it can properly label the commits you make:

     git config --global user.name "Your Name Here"

Git also saves your email address into the commits you make. We use the email address to associate your commits with your GitHub account:

     git config --global user.email "your-email@youremail.com"


### GIT Tutorial
If you have never worked with git before, we recommend to go through the following basic git tutorial:

     http://excess.org/article/2008/07/ogre-git-tutorial/


### ROS - Robot Operating System
#### Install ROS
The repository has been tested successfully with the following ROS distributions. Use the link behind a ROS distribution to get to the particular ROS installation instructions.

- ROS Hydro - http://www.ros.org/wiki/Hydro/Installation/Ubuntu

NOTE: Do not forget to update your .bashrc!

#### ROS Tutorials
If you have never worked with ROS before, we recommend to go through the beginner tutorials provided by ROS:

     http://www.ros.org/wiki/ROS/Tutorials

In order to understand at least the different core components of ROS, you have to start from tutorial 1 ("Installing and Configuring Your ROS Environment") till tutorial 7 ("Understanding ROS Services and Parameters"). 

#### Set up a catkin workspace

    mkdir -p ~/catkin_ws/src; cd ~/catkin_ws/src
    catkin_init_workspace
    cd ..
    catkin_make
    source ~/catkin_ws/devel/setup.bash
    export ROS_PACKAGE_PATH=~/catkin_ws/devel:$ROS_PACKAGE_PATH

The last two commands, should be added to the ~/.bashrc file, so that they do not need to be executed everytime you open a new terminal.

### Clone and compile the zeno robot software
First of all you have to clone the repository.

    cd ~/catkin_ws/src;
    git clone git@bitbucket.org:shehzi001/zeno_robot.git

Then go on with installing further external dependencies:
       
    cd ~/catkin_ws/src/zeno_robot
    ./repository.debs

And finally compile the repository:

    cd ~/catkin_ws
    catkin_make

If no errors appear everything is ready to use. Great job!

### Controlling Zeno using robokind api interface

#### Clone following repository
Follow instruction given in the README of the repository to launch robokind api.

    git clone git@bitbucket.org:shehzi001/zeno_robokind_api_interface.git
    
#### Launch joint controller interface to robokind api

    roslaunch zeno_bringup robot_robokind_interface.launch
    
#### Test using rqt ui interface
    
    rosrun rqt_reconfigure rqt_reconfigure

### Controlling Zeno directly remotly without robokind api

## Setting the Environment Variables
#### Setting up virtual serial ports

     echo "export SERIAL_PORT_HEAD=~/dev/ttyzenohead" >> ~/.bashrc
     echo "export SERIAL_PORT_BODY=~/dev/ttyzenobody" >> ~/.bashrc

#### Setting up ip to host name mapping for zeno

    Get the ip address and host name of the zeno.
        e.g; ip:192.168.0.1 and host name = zeno
    Add a line in /etc/hosts file:
       192.168.0.1 zeno

#### Make sure that remote computer and zeno are connected to the same network.

## Initialize zeno serial port connections using socat
This command will create two virtual serial ports(ttyzenohead and ttyzenobody) under ~/dev/

    roscd zeno_joint_controller/doc && ./socat_init.sh
    Note: If you get error then there are other scripts provided to kill socat process on remote workstation locally and zeno's pc. e.g.
    roscd zeno_joint_controller/doc && ./socat_zeno_kill.sh 
    or 
    roscd zeno_joint_controller/doc && ./socat_local_kill.sh 
     
    and run the command again (./socat_init.sh)
    
## Launch joint controller interface to real robot
If tty ports for the body and head are setup properly. Then launch joint controller interface.

    roslaunch zeno_bringup robot.launch

### Test the head and body joints.

     rosrun rqt_reconfigure rqt_reconfigure

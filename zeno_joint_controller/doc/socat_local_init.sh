#!/bin/sh
mkdir -p ~/dev

socat pty,link=$SERIAL_PORT_BODY tcp-connect:zeno:5000 &
socat pty,link=$SERIAL_PORT_HEAD,waitslave tcp:zeno:3003 &

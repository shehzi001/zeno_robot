#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
This script provides an implementation of zeno joint controller for simulation with avatar.
"""
__author__ = 'shehzad ahmed'
import rospy
from zeno_body_joint_controller_sim import *
from zeno_head_joint_controller_sim import *

from thread import start_new_thread

def run_body_controller():
    body_joint_controller = ZenoBodyJointController()
    loop_rate = rospy.Rate(5)

    rospy.loginfo("body controller is running")

    while not rospy.is_shutdown():
        #body_joint_controller.publish_joint_states()
        loop_rate.sleep()

    rospy.loginfo("body controller thread down")

def run_head_controller():
    head_joint_controller = ZenoHeadJointController()

    loop_rate = rospy.Rate(5)

    rospy.loginfo("head controller is running")

    while not rospy.is_shutdown():
        head_joint_controller.publish_joint_states()

        loop_rate.sleep()

    rospy.loginfo("head controller thread down")


def initialize_node():
    rospy.init_node('zeno_joint_controller_node', anonymous=False)

    start_new_thread(run_body_controller,())
    start_new_thread(run_head_controller,())

    rospy.loginfo("zeno_joint_controller_node is now running")

    while not rospy.is_shutdown():
        rospy.sleep(1)

if __name__ == '__main__':
    initialize_node()

#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
This script provides an implementation of zeno joint controller for simulation with avatar.
"""
__author__ = 'shehzad ahmed'
import rospy

from zeno_joint_controller_simulation import *

from thread import start_new_thread

def run_controller():
    joint_controller = ZenoJointController()
    loop_rate = rospy.Rate(5)

    while not rospy.is_shutdown():
        #body_joint_controller.publish_joint_states()
        joint_controller.publish_joint_command()
        loop_rate.sleep()

    rospy.loginfo("joint controller thread down")

def initialize_node():
    rospy.init_node('zeno_joint_controller_node', anonymous=False)

    start_new_thread(run_controller,())

    rospy.loginfo("Joint controller robokind interface node is running")

    while not rospy.is_shutdown():
        rospy.sleep(1)

if __name__ == '__main__':
    initialize_node()

#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
This script provides an implementation of zeno head joint controller.
"""
__author__ = 'shehzad ahmed'
import rospy

import time 
from zeno_head_controller import *
from trajectory_msgs.msg import JointTrajectory, JointTrajectoryPoint
from sensor_msgs.msg import JointState

import numpy
import json

class ZenoHeadJointController():
    def __init__(self):
        self.is_head_controller_active = False
        self.head_joint_trajectory_sub = rospy.Subscriber("/head_controller/trajectory_command", 
                                    JointTrajectory, 
                                    self.head_joint_trajectory_cb, queue_size=1)
        self.joint_states_pub = rospy.Publisher("joint_states", JointState, queue_size=1)
        
        self.initization()

        self.initilization_msgs()

        self.initialize_joints()

    def head_joint_trajectory_cb(self, msg):
        len_joints_names = len(msg.joint_names)
        if (len_joints_names != 0):
            len_trajectory_points = len(msg.points)
            for point in range(0,len_trajectory_points):
                joint_angles = msg.points[point].positions

                self.head_controller.set_joint_position_commands(msg.joint_names, joint_angles)
        else:
            print "Empty trajectory message."

    def initization(self):
        if(not(self.is_head_controller_active)):
            self.is_head_controller_active = self.initialize_head_controller()


    def initilization_msgs(self):
        success = True
        if(not(self.is_head_controller_active)):
            print "Failed to initialize head controller."
            success = False

        if(not(success)):
            print "Controller will keep trying to initialize."

    def initialize_head_controller(self):
        self.head_controller = ZenoHeadController()
        success = self.head_controller.initialization()
        if (not(success)):
            return success

        print "Head controller is initialized...." 

        return success

    def publish_joint_states(self):
        names,positions = self.head_controller.get_joint_states()
        joint_state = JointState()

        joint_state.name = names
        joint_state.position = positions

        self.joint_states_pub.publish(joint_state)

    def initialize_joints(self):
        if(self.is_head_controller_active):
            self.initialize_head_joints()

    def initialize_head_joints(self):
        success = self.head_controller.initialize_joints()
        if(not(success)):
            print "Failed to initialize head joints."
            return success

        return success

    def shutdown(self):
        print "Shutting down joint controller.."
        if(self.is_head_controller_active):
            self.head_controller.shutdown()

    def __del__(self):
        self.shutdown()

#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
This script provides an implementation of zeno body joint controller.
"""
__author__ = 'shehzad ahmed'
import rospy

import time 
from zeno_body_controller import *
from trajectory_msgs.msg import JointTrajectory, JointTrajectoryPoint
from sensor_msgs.msg import JointState
from std_msgs.msg import Bool

import numpy
import json

class ZenoBodyJointController():
    def __init__(self):
        self.is_body_controller_active = False
        self.body_joint_trajectory_sub = rospy.Subscriber("/body_controller/trajectory_command", 
                                    JointTrajectory, 
                                    self.body_joint_trajectory_cb, queue_size=1)

        self.activate_deactivate_joints_sub = rospy.Subscriber("/body_controller/activate_deactivate", 
                                    Bool, 
                                    self.activate_deactivate_joints_cb, queue_size=1)

        self.enable_body_joints_sub = rospy.Subscriber("/body_controller/enable_joints", 
                                    JointState, 
                                    self.enable_body_joints_cb, queue_size=1)

        self.disable_body_joints_sub = rospy.Subscriber("/body_controller/disable_joints", 
                                    JointState, 
                                    self.distable_body_joints_cb, queue_size=1)

        self.joint_states_pub = rospy.Publisher("joint_states", JointState, queue_size=1)
        
        self.initization()

        self.initilization_msgs()

        #self.initialize_joints()

    def body_joint_trajectory_cb(self, msg):
        len_joints_names = len(msg.joint_names)
        if (len_joints_names != 0):
            len_trajectory_points = len(msg.points)
            for point in range(0,len_trajectory_points):
                joint_angles = msg.points[point].positions

                self.body_controller.set_joint_position_commands(msg.joint_names, joint_angles)
        else:
            print "Empty trajectory message."

    def activate_deactivate_joints_cb(self, msg):
        if (msg.data):
                self.body_controller.enable_all_joints()
        elif (not(msg.data)):
                self.body_controller.disable_all_joints()

    def enable_body_joints_cb(self, msg):
        len_joints_names = len(msg.name)
        if (len_joints_names != 0):
                self.body_controller.enable_joints(msg.name)
        else:
            print "Empty trajectory message."

    def distable_body_joints_cb(self, msg):
        len_joints_names = len(msg.name)
        if (len_joints_names != 0):
                self.body_controller.disable_joints(msg.name)
        else:
            print "Empty trajectory message."

    def initization(self):

        if(not(self.is_body_controller_active)):
            self.is_body_controller_active = self.initialize_body_controller()


    def initilization_msgs(self):
        success = True
        if(not(self.is_body_controller_active)):
            print "Failed to initialize body controller."
            success = False

        if(not(success)):
            print "Controller will keep trying to initialize."

    def initialize_body_controller(self):
        self.body_controller = ZenobodyController()
        success = self.body_controller.initialization()
        if (not(success)):
            return success

        print "Body controller is initialized...." 

        return success

    def publish_joint_states(self):
        names,positions,velocities,efforts = self.body_controller.get_joint_states()
        joint_state = JointState()

        joint_state.name = names
        joint_state.position = positions 
        joint_state.velocity = velocities
        joint_state.effort = efforts

        self.joint_states_pub.publish(joint_state)

    def initialize_joints(self):
        if(self.is_body_controller_active):
            self.initialize_body_joints()

    def initialize_body_joints(self):
        success = self.body_controller.initialize_joints()
        if(not(success)):
            print "Failed to initialize body joints."
            return success

        return success

    def shutdown(self):
        print "Shutting down joint controller.."
        if(self.is_body_controller_active):
            self.body_controller.shutdown()

    def __del__(self):
        self.shutdown()

#!/usr/bin/python
import sys
import time
import os

import json

from zeno_head_configuration import zeno_head_config as zhc
from zeno_head_motors_interface import *

class ZenoHeadController():
    def __init__(self):
        self.is_head_motor_interface_running = False
        self.mock_up = False

    def initialization(self):
        #print "Info: Initializing head motors Interface....."
        if(self.mock_up):
            return True

        env_var_name_port = zhc['controllers']['head_controller']['env_port']

        self.serial_port_name = os.environ.get(env_var_name_port)
        if (self.serial_port_name == None):
            print 'Environment variable', env_var_name_port , "doesn't exit." 
            return False

        self.serial_baud_rate = zhc['controllers']['head_controller']['baud_rate']
        self.default_speed  = zhc['controllers']['head_controller']['default_joint_speed']

        success = self.initilize_head_motors_interface()

        return success

    def initilize_head_motors_interface(self):
        self.head_motors_interface = ZenoHeadMotorsInterface(self.serial_port_name, self.serial_baud_rate)
        success = self.head_motors_interface.initilize_head_interface()

        if(not success):
            print('initilize_head_interface failed')
            return success

        return success

    def initialize_joints(self):
        if(self.mock_up):
            return True

        joint_names = self.get_joint_group('head')
        joint_speeds = [self.default_speed] * len(joint_names)

        #print 'Info: Calibrating head joints.'
        
        success = self.set_joint_velocity_commands(joint_names, joint_speeds)
        if(not success):
            print 'Error: Could not set joint speeds'
            return False

        time.sleep(0.1)

        success = self.head_motors_interface.go_home()
        if(not success):
            print 'Error: Could not go home position.'
            return False

        return success

    def set_joint_position_commands(self, joint_names, joint_angles):
        print "Head Position command recevied."
        if(self.mock_up):
            return True

        number_of_joint_cmds = len(joint_names)
        loop_index = 0

        while loop_index < number_of_joint_cmds:
            joint_name = joint_names[loop_index]
            joint_angle = joint_angles[loop_index]

            motor_angle_range = zhc['motors'][joint_name]['angle_limit']
            maximum_angle_limit = motor_angle_range[1]
            motor_angle_offset = zhc['motors'][joint_name]['angle_offset']

            if joint_angle <=  maximum_angle_limit:

                joint_id = self.get_joint_id(joint_name)
                joint_angle_with_offset = int(joint_angle) + motor_angle_offset

                success = self.head_motors_interface.set_motor_position_command(1, joint_id, joint_angle_with_offset)

                if(not success):
                    break

            else:
                print 'Warning:' + joint_name + ' angle is out of limit.'
                break

            loop_index = loop_index + 1

        return True

    def set_joint_velocity_commands(self, joint_names, joint_speeds):
        if(self.mock_up):
            return True

        number_of_joint_cmds = len(joint_names)
        loop_index = 0

        while loop_index < number_of_joint_cmds:
            joint_name = joint_names[loop_index]
            joint_speed = joint_speeds[loop_index]

            joint_id = self.get_joint_id(joint_name)

            success = self.head_motors_interface.set_motor_velocity_command(joint_id, joint_speed)

            if(not success):
                break

            loop_index = loop_index + 1

        return True

    def get_joint_states(self):
        joint_names = self.get_joint_group('head')
        joint_position = []

        if self.mock_up:
            joint_position = [0.0]*len(joint_names)
            return joint_names, joint_position
        
        position = 0.0

        for joint_name in joint_names:
            joint_id = self.get_joint_id(joint_name)
            motor_pwm = self.head_motors_interface.get_pwm_time(joint_id)

            pwm_with_offset = motor_pwm - self.get_pwm_offset(joint_name)
            position_limits = self.get_position_range(joint_name)
            total_angle = position_limits[1] - position_limits[0]
            max_pwm_time = self.get_max_pwm_time(joint_name)
            position = total_angle*(float(pwm_with_offset)/max_pwm_time) - (total_angle/2)
            joint_position.append(position)

        return joint_names, joint_position

    def get_joint_id(self, joint_name):
        return zhc['motors'][joint_name]['id']

    def get_joint_group(self, group_name):
        return zhc['motorgroups'][group_name]

    def get_position_range(self, joint_name):
        position_range = zhc['motors'][joint_name]['angle_limit']
        return position_range

    def get_max_pwm_time(self, joint_name):
        pwm_time_range = zhc['motors'][joint_name]['pwm_time_limit']
        return pwm_time_range[1]

    def get_pwm_offset(self, joint_name):
        return zhc['motors'][joint_name]['pwm_time_offset']

    def shutdown(self):
        if(not(self.mock_up)):
            self.head_motors_interface.shutdown()

    def __del__(self):
        self.shutdown()

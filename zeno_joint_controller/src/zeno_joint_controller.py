#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
This script provides an implementation of zeno joint controller.
"""
__author__ = 'shehzad ahmed'
import rospy
from zeno_body_joint_controller import *
from zeno_head_joint_controller import *

from thread import start_new_thread

def run_body_controller():
    body_joint_controller = ZenoBodyJointController()
    loop_rate = rospy.Rate(5)
    rospy.loginfo("body controller running")
    while not rospy.is_shutdown():
        body_joint_controller.publish_joint_states()
        loop_rate.sleep()

    body_joint_controller.shutdown()
    rospy.loginfo("body controller thread down")

def run_head_controller():
    head_joint_controller = ZenoHeadJointController()
    loop_rate = rospy.Rate(5)
    rospy.loginfo("head controller running")
    while not rospy.is_shutdown():
        head_joint_controller.publish_joint_states()
        loop_rate.sleep()

    head_joint_controller.shutdown()
    rospy.loginfo("head controller thread down")


def initlize_node():
    rospy.init_node('zeno_joint_controller_node', anonymous=False)
    rospy.loginfo("zeno_joint_controller_node is now running")

    start_new_thread(run_body_controller,())
    start_new_thread(run_head_controller,())

    while not rospy.is_shutdown():
        rospy.sleep(1)

if __name__ == '__main__':
    initlize_node()

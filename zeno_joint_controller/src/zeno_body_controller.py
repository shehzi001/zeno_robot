#!/usr/bin/python
import os
from zeno_body_config import *
import pypot.robot
import time 
import pypot.dynamixel
from zeno_body_config import zeno_robot_config as zrc
import json
import numpy

class ZenobodyController():
    def __init__(self):
        self.is_controller_initialized =  False
        self.mock_up = run_mock_up
        
        #self.position_cmd_execution_time = position_command_execution_time

    def initialization(self):
        if(self.mock_up):
            return True

        self.joint_names =  zrc['motorgroups']['robot']
        env_var_name_port = zrc['controllers']['body_controller']['env_port']
        self.DOF = zrc['controllers']['body_controller']['DOF']
        self.default_joint_speeds = zrc['controllers']['body_controller']['default_joint_speeds']

        self.port = os.environ.get(env_var_name_port)
        if (self.port == None):
            print 'Environment variable', env_var_name_port , "doesn't exit." 
            return False

        try:
            self.dxl_io = pypot.dynamixel.DxlIO(self.port)
            self.motors = self.dxl_io.scan(range(0, self.DOF))
            if (len(self.motors) == 0):
                return False
            time.sleep(1.0)
        except:
            print 'Could not initilize connection with ', self.port
            return False

        try:
            self.initialize_joints()
        except ValueError:
            print 'Joint initilization error:',ValueError
            return False

        self.is_controller_initialized = True
        return True

    def initialize_joints(self):
        joint_velocities = [default_joint_speeds] * len(self.motors)
        self.set_velocities(self.motors, joint_velocities)
        self.enable_all_joints()
        return True

    def set_velocities(self, joint_ids, joint_velocities):
        if(self.mock_up):
            return True
        try:
            velocity_command = dict(zip(joint_ids, joint_velocities))
            self.dxl_io.set_moving_speed(velocity_command)
        except:
            return False

        return True

    def get_joint_states(self):
        names = self.joint_names
        positions = []
        velocities = []
        efforts = []

        if self.mock_up:
            positions = [0.0]*len(names)
            velocities = [0.0]*len(names)
            efforts = [0.0]*len(names)
            return names,positions,velocities,efforts

        try:
            positions = self.dxl_io.get_present_position(self.motors)
            #time.sleep(0.1)
            #velocities = self.dxl_io.get_present_speed(self.motors)
            #time.sleep(0.1)
            #efforts = self.dxl_io.get_present_load(self.motors)
        except:
            pass

        positions = list(numpy.radians(positions))
        velocities = list(numpy.radians(velocities))
        efforts = list(numpy.radians(efforts))
        return names, positions, velocities, efforts

    def set_joint_position_commands(self, joint_names, joint_angle):
        print "Body Position command recevied."
        joint_angle = list(numpy.degrees(joint_angle))

        if(self.mock_up):
            return True
        try:
            joint_id = self.get_joints_id(joint_names)
            if len(joint_id) == len(joint_angle):
                position_command = dict(zip(joint_id, joint_angle))
                #print(position_command)
                self.dxl_io.set_goal_position(position_command)
        except:
            return False

        return True

    def enable_all_joints(self):
        self.enable_joints(self.joint_names)

    def disable_all_joints(self):
        self.disable_joints(self.joint_names)

    def enable_joints(self, joint_names):
        if(self.mock_up):
            return True
        try:
            joint_id_list = self.get_joints_id(joint_names)
            self.dxl_io.enable_torque(joint_id_list)
        except:
            return False

        return True

    def disable_joints(self, joint_names):
        if(self.mock_up):
            return True
        try:
            joint_id_list = self.get_joints_id(joint_names)
            self.dxl_io.disable_torque(joint_id_list)
        except:
            return False

        return True

    def get_joint_id(self, joint_name):
        return zrc['motors'][joint_name]['id']

    def get_joints_id(self, joint_names):
        joint_ids = []
        for m in joint_names:
            joint_ids.append(self.get_joint_id(m))

        return joint_ids

    def shutdown(self):
        if(self.is_controller_initialized and not(self.mock_up)):
            self.dxl_io.close()

    def __del__(self):
        self.shutdown()

#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
This script provides an implementation of zeno head joint controller for simulation with avatar.
"""
__author__ = 'shehzad ahmed'
import rospy

import time 
from zeno_head_controller import *
from trajectory_msgs.msg import JointTrajectory, JointTrajectoryPoint
from sensor_msgs.msg import JointState
from zeno_head_configuration import zeno_head_config as zhc

import numpy
import json

class ZenoHeadJointController():
    def __init__(self):
        self.is_head_controller_active = False
        self.head_joint_trajectory_sub = rospy.Subscriber("/head_controller/trajectory_command", 
                                    JointTrajectory, 
                                    self.head_joint_trajectory_cb, queue_size=1)

        self.head_joint_trajectory_pub_sim = rospy.Publisher("/head_controller/trajectory_command_sim", 
                                    JointTrajectory, queue_size=1)

        #self.joint_states_pub = rospy.Publisher("joint_states", JointState, queue_size=1)
        
        self.initization()

        self.initilization_msgs()

        self.initialize_joints()

    def head_joint_trajectory_cb(self, msg):
        len_joints_names = len(msg.joint_names)
        if (len_joints_names != 0):
            len_trajectory_points = len(msg.points)
            for point in range(0,len_trajectory_points):
                joint_angles = msg.points[point].positions

                msg.points[point].positions = self.normalize_joint_angles(msg.joint_names, msg.points[point].positions)

            self.head_joint_trajectory_pub_sim.publish(msg)
        else:
            print "Empty trajectory message."

    def normalize_joint_angles(self, joint_names, joint_positions):
        #Normalized Data
        #joint_positions = list(numpy.degrees(joint_positions))
        joint_positions = list(joint_positions)
        for index, name in enumerate(joint_names):
            joint_limits = zhc['motors'][name]['angle_limit']
            lim_min = joint_limits[1]
            lim_max = joint_limits[0]

            joint_positions[index] = ((joint_positions[index]- lim_min)/(lim_max - lim_min))

            if joint_positions[index] <=  0.0:
                joint_positions[index] = 0.01
            elif joint_positions[index] >=  1.0:
                joint_positions[index] = 0.99

        return joint_positions

    def initization(self):
        if(not(self.is_head_controller_active)):
            self.is_head_controller_active = self.initialize_head_controller()


    def initilization_msgs(self):
        success = True
        if(not(self.is_head_controller_active)):
            print "Failed to initialize head controller."
            success = False

        if(not(success)):
            print "Controller will keep trying to initialize."

    def initialize_head_controller(self):
        success = True
        if (not(success)):
            return success

        print "Head controller is initialized...." 

        return success

    def publish_joint_states(self):
        '''
        names,positions = self.head_controller.get_joint_states()
        joint_state = JointState()

        joint_state.name = names
        joint_state.position = positions

        self.joint_states_pub.publish(joint_state)
        '''

    def initialize_joints(self):
        if(self.is_head_controller_active):
            self.initialize_head_joints()

    def initialize_head_joints(self):
        success = True
        if(not(success)):
            print "Failed to initialize head joints."
            return success

        return success

    def shutdown(self):
        print "Shutting down joint controller.."
        if(self.is_head_controller_active):
            pass

    def __del__(self):
        self.shutdown()

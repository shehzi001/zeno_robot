#!/usr/bin/python

run_mock_up = False
default_joint_speeds = 20
position_command_execution_time = 0.75

zeno_robot_config = {
    'controllers': {
        'body_controller': {
            'env_port': 'SERIAL_PORT_BODY',
            'sync_read': False,
            'DOF': 21,
            'default_joint_speeds': 40, # deg/sec
            'position_command_execution_time': 0.75, #sec
            'attached_motors': ['robot', 'left_arm','base', 
                                'right_arm','left_leg', 'right_leg'],
        },
    },

    'motorgroups': {
        'robot': ['waist','left_shoulder_pitch', 'left_shoulder_roll',
                    'left_elbow_roll', 'left_elbow_yaw',
                    'right_shoulder_pitch', 'right_shoulder_roll', 
                    'right_elbow_roll', 'right_elbow_yaw',
                    'left_hip_roll', 'left_hip_yaw', 'left_hip_pitch',
                    'left_knee_pitch', 'left_ankle_pitch', 'left_ankle_roll',
                    'right_hip_roll', 'right_hip_yaw', 'right_hip_pitch',
                    'right_knee_pitch', 'right_ankle_pitch', 'right_ankle_roll'],
        'base': ['waist'],
        'left_arm': ['left_shoulder_pitch', 'left_shoulder_roll', 
                    'left_elbow_roll', 'left_elbow_yaw'],
        'right_arm': ['right_shoulder_pitch', 'right_shoulder_roll', 
                    'right_elbow_roll', 'right_elbow_yaw'],
        'left_leg': ['left_hip_roll', 'left_hip_yaw', 'left_hip_pitch', 
                    'left_knee_pitch', 'left_ankle_pitch', 'left_ankle_roll'],
        'right_leg': ['right_hip_roll', 'right_hip_yaw', 'right_hip_pitch', 
                    'right_knee_pitch', 'right_ankle_pitch', 'right_ankle_roll'],
    },

    'motors': {
        'waist': {
            'id': 0,
            'type': 'RX-28',
            'orientation': 'direct',
            'offset': 0.0,
            'angle_limit': (-60.0, 60.0),
        },
        'left_shoulder_pitch': {
            'id': 1,
            'type': 'RX-28',
            'orientation': 'indirect',
            'offset': 0.0,
            'angle_limit': (-90, 90),
        },
        'left_shoulder_roll': {
            'id': 2,
            'type': 'RX-28',
            'orientation': 'direct',
            'offset': 0.0,
            'angle_limit': (-81.38, 7.48),
        },
        'left_elbow_roll': {
            'id': 3,
            'type': 'RX-28',
            'orientation': 'indirect',
            'offset': 0.0,
            'angle_limit': (-90.0, 90.0),
        },
        'left_elbow_yaw': {
            'id': 4,
            'type': 'RX-28',
            'orientation': 'direct',
            'offset': 0.0,
            'angle_limit': (0.0, 90.0),
        },
        'right_shoulder_pitch': {
            'id': 5,
            'type': 'RX-28',
            'orientation': 'direct',
            'offset': 0.0,
            'angle_limit': (-90, 90),
        },
        'right_shoulder_roll': {
            'id': 6,
            'type': 'RX-28',
            'orientation': 'indirect',
            'offset': 0.0,
            'angle_limit': (-6.01, 87.0),
        },
        'right_elbow_roll': {
            'id': 7,
            'type': 'RX-28',
            'orientation': 'direct',
            'offset': 0.0,
            'angle_limit': (-90.0, 90.0),
        },
        'right_elbow_yaw': {
            'id': 8,
            'type': 'RX-28',
            'orientation': 'direct',
            'offset': 0.0,
            'angle_limit': (0.0, 90.0),
        },

        'left_hip_roll': {
            'id': 9,
            'type': 'RX-64',
            'orientation': 'direct',
            'offset': 0.0,
            'angle_limit': (25.0, 70.0),
        },
        'left_hip_yaw': {
            'id': 10,
            'type': 'RX-28',
            'orientation': 'indirect',
            'offset': 0,
            'angle_limit': (-90.0, 90.0),
        },
        'left_hip_pitch': {
            'id': 11,
            'type': 'RX-64',
            'orientation': 'direct',
            'offset': 0.0,
            'angle_limit': (0.0, 90.0),
        },
        'left_knee_pitch': {
            'id': 12,
            'type': 'RX-64',
            'orientation': 'direct',
            'offset': 0.0,
            'angle_limit': (0.0, 90.0),
        },
        'left_ankle_pitch': {
            'id': 13,
            'type': 'RX-64',
            'orientation': 'direct',
            'offset': 0.0,
            'angle_limit': (-45.0, 75.0),
        },
        'left_ankle_roll': {
            'id': 14,
            'type': 'RX-64',
            'orientation': 'indirect',
            'offset': 0.0,
            'angle_limit': (-25.0, 25.0),
        },
        'right_hip_roll': {
            'id': 15,
            'type': 'RX-64',
            'orientation': 'indirect',
            'offset': 0.0,
            'angle_limit': (-20.0, 25.0),
        },

        'right_hip_yaw': {
            'id': 16,
            'type': 'RX-28',
            'orientation': 'direct',
            'offset': 0.0,
            'angle_limit': (-90.0, 90.0),
        },
        'right_hip_pitch': {
            'id': 17,
            'type': 'RX-64',
            'orientation': 'direct',
            'offset': 0.0,
            'angle_limit': (0.0, 90.0),
        },
        'right_knee_pitch': {
            'id': 18,
            'type': 'RX-64',
            'orientation': 'direct',
            'offset': 0.0,
            'angle_limit': (0.0, 90.0),
        },
        'right_ankle_pitch': {
            'id': 19,
            'type': 'RX-64',
            'orientation': 'direct',
            'offset': 0.0,
            'angle_limit': (-45.0, 75.0),
        },
        'right_ankle_roll': {
            'id': 20,
            'type': 'RX-64',
            'orientation': 'direct',
            'offset': 0.0,
            'angle_limit': (-25.0, 25.0),
        },
    },
}

#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
This script provides an implementation of zeno body joint controller for simulation with avatar.
"""
__author__ = 'shehzad ahmed'
import rospy

import time 
from zeno_body_controller import *
from trajectory_msgs.msg import JointTrajectory, JointTrajectoryPoint
from sensor_msgs.msg import JointState
from std_msgs.msg import Bool
from zeno_body_config import zeno_robot_config as zrc
from zeno_head_configuration import zeno_head_config as zhc

import numpy
import json

class ZenoJointController():
    def __init__(self):

        self.body_joint_trajectory_sub = rospy.Subscriber("/body_controller/trajectory_command", 
                                    JointTrajectory, 
                                    self.body_joint_trajectory_cb, queue_size=10)

        self.head_joint_trajectory_sub = rospy.Subscriber("/head_controller/trajectory_command", 
                            JointTrajectory, 
                            self.head_joint_trajectory_cb, queue_size=10)

        self.body_joint_trajectory_pub_sim = rospy.Publisher("/body_controller/trajectory_command_sim", 
                                    JointTrajectory, queue_size=1)

        self.joint_states_pub = rospy.Publisher("joint_states", JointState, queue_size=1)

        self.head_joint_names = None 
        self.head_joint_trajectory_points = None

        self.body_joint_names = None
        self.body_joint_trajectory_points = None

    def body_joint_trajectory_cb(self, msg):

        len_joints_names = len(msg.joint_names)

        if (len_joints_names != 0):
            len_trajectory_points = len(msg.points)
            for point in range(0,len_trajectory_points):
                self.publish_joint_states(msg.joint_names, msg.points[point].positions)
                msg.points[point].positions = self.normalize_body_joint_angles(msg.joint_names, msg.points[point].positions)

            self.body_joint_trajectory_pub_sim.publish(msg)
            #self.body_joint_names = msg.joint_names
            #self.body_joint_trajectory_points = msg.points

        else:
            pass

    def head_joint_trajectory_cb(self, msg):
        len_joints_names = len(msg.joint_names)
        if (len_joints_names != 0):
            len_trajectory_points = len(msg.points)
            for point in range(0,len_trajectory_points):
                joint_angles = msg.points[point].positions

                msg.points[point].positions = self.normalize_head_joint_angles(msg.joint_names, msg.points[point].positions)

            #self.head_joint_trajectory_pub_sim.publish(msg)
            self.head_joint_names = msg.joint_names
            self.head_joint_trajectory_points = msg.points
        else:
            pass
            #print "Empty trajectory message."

    def normalize_body_joint_angles(self, joint_names, joint_positions):
        #Normalized Data
        joint_positions = list(numpy.degrees(joint_positions))

        for index, name in enumerate(joint_names):
            joint_limits = zrc['motors'][name]['angle_limit']
            joint_orientation = zrc['motors'][name]['orientation']
            if (joint_orientation == "direct"):
                lim_min = joint_limits[0]
                lim_max = joint_limits[1]
            else:
                lim_max = joint_limits[0]
                lim_min = joint_limits[1]

            joint_positions[index] = ((joint_positions[index]- lim_min)/(lim_max - lim_min))

            '''
            if joint_positions[index] <=  0.0:
                joint_positions[index] = 0.01
            elif joint_positions[index] >=  1.0:
                joint_positions[index] = 0.99
            '''

        return joint_positions

    def normalize_head_joint_angles(self, joint_names, joint_positions):
        #Normalized Data
        joint_positions = list(joint_positions)

        for index, name in enumerate(joint_names):
            joint_limits = zhc['motors'][name]['angle_limit']
            lim_min = joint_limits[1]
            lim_max = joint_limits[0]

            joint_positions[index] = ((joint_positions[index]- lim_min)/(lim_max - lim_min))
            if joint_positions[index] <=  0.0:
                joint_positions[index] = 0.01
            elif joint_positions[index] >=  1.0:
                joint_positions[index] = 0.99

        return joint_positions

    def publish_joint_command(self):

        joint_names = []
        trajectory_points = []

        if (self.head_joint_names is not None) and \
            self.head_joint_trajectory_points is not None:
            joint_names = joint_names + self.head_joint_names
            trajectory_points = trajectory_points + self.head_joint_trajectory_points

            self.head_joint_names = None
            self.head_joint_trajectory = None


        if (self.body_joint_names is not None) and \
            self.body_joint_trajectory_points is not None:
            joint_names = joint_names + self.body_joint_names
            trajectory_points = trajectory_points + self.body_joint_trajectory_points

            self.body_joint_names = None
            self.body_joint_trajectory_points = None

        #publish joint commands

        if len(joint_names) is not 0 and len(trajectory_points) is not 0:
            joint_traj = JointTrajectory()
            joint_traj.joint_names = joint_names
            joint_traj.points = trajectory_points

            self.body_joint_trajectory_pub_sim.publish(joint_traj)

    def publish_joint_states(self, joint_names, joint_positions):
        joint_state = JointState()
        joint_state.name = joint_names
        joint_state.position = joint_positions 
        joint_state.velocity = [0]*len(joint_names)
        joint_state.effort = [0]*len(joint_names)

        self.joint_states_pub.publish(joint_state)

    def shutdown(self):
        print "Shutting down joint controller.."

    def __del__(self):
        self.shutdown()

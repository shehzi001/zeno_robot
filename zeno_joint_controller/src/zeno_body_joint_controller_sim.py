#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
This script provides an implementation of zeno body joint controller for simulation with avatar.
"""
__author__ = 'shehzad ahmed'
import rospy

import time 
from zeno_body_controller import *
from trajectory_msgs.msg import JointTrajectory, JointTrajectoryPoint
from sensor_msgs.msg import JointState
from std_msgs.msg import Bool
from zeno_body_config import zeno_robot_config as zrc

import numpy
import json

class ZenoBodyJointController():
    def __init__(self):
        self.is_body_controller_active = False
        self.body_joint_trajectory_sub = rospy.Subscriber("/body_controller/trajectory_command", 
                                    JointTrajectory, 
                                    self.body_joint_trajectory_cb, queue_size=1)

        self.body_joint_trajectory_pub_sim = rospy.Publisher("/body_controller/trajectory_command_sim", 
                                    JointTrajectory, queue_size=1)

        self.joint_states_pub = rospy.Publisher("joint_states", JointState, queue_size=1)
        
        self.initization()

        self.initilization_msgs()

    def body_joint_trajectory_cb(self, msg):
        #print "eventin_cb message:", msg.joint_names
        #joint_angles_rad = list(msg.points[0].positions)
        #joint_angles_deg = list(numpy.degrees(joint_angles_rad))
        #print "Recv message....."
        joint_states_positions = []
        len_joints_names = len(msg.joint_names)
        if (len_joints_names != 0):
            len_trajectory_points = len(msg.points)
            for point in range(0,len_trajectory_points):
                self.publish_joint_states(msg.joint_names, msg.points[point].positions)
                msg.points[point].positions = self.normalize_joint_angles(msg.joint_names, msg.points[point].positions)

            self.body_joint_trajectory_pub_sim.publish(msg)
        else:
            print "Empty trajectory message."

    def normalize_joint_angles(self, joint_names, joint_positions):
        #Normalized Data
        joint_positions = list(numpy.degrees(joint_positions))
        for index, name in enumerate(joint_names):
            joint_limits = zrc['motors'][name]['angle_limit']
            joint_orientation = zrc['motors'][name]['orientation']
            if (joint_orientation == "direct"):
                lim_min = joint_limits[0]
                lim_max = joint_limits[1]
            else:
                lim_max = joint_limits[0]
                lim_min = joint_limits[1]

            joint_positions[index] = ((joint_positions[index]- lim_min)/(lim_max - lim_min))

        return joint_positions

    def initization(self):

        if(not(self.is_body_controller_active)):
            self.is_body_controller_active = True

        print "Body controller is initialized...." 


    def initilization_msgs(self):
        success = True
        if(not(self.is_body_controller_active)):
            print "Failed to initialize body controller."
            success = False

        if(not(success)):
            print "Controller will keep trying to initialize."

    def publish_joint_states(self, joint_names, joint_positions):
        joint_state = JointState()
        joint_state.name = joint_names
        joint_state.position = joint_positions 
        joint_state.velocity = [0]*len(joint_names)
        joint_state.effort = [0]*len(joint_names)

        self.joint_states_pub.publish(joint_state)

    def shutdown(self):
        print "Shutting down joint controller.."
        if(self.is_body_controller_active):
            pass

    def __del__(self):
        self.shutdown()

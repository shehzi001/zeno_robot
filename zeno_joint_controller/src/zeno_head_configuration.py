#!/usr/bin/python
# -*- coding: utf-8 -*-
#Mini-SSC protocol: 0xFF, channel address, 8-bit target

#Set Multiple Targets (Mini Maestro 12, 18, and 24 only)
#Compact protocol: 0x9F, number of targets, first channel number, first target low bits, first target high bits, second target low bits, second target high bits, …
#Pololu protocol: 0xAA, device number, 0x1F, number of targets, first channel number, first target low bits, first target high bits, second target low bits, second target high bits


set_position_ssc_single = 0xFF
set_position_compact_single = 0x84
set_position_pololu_single = 0x04


set_position_compact_multiple = 0x9F
set_position_pololu_multiple = 0x1F

set_speed_compact = 0x87
set_speed_pololu = 0x07

set_acceleration_compact =  0x89
set_acceleration_pololu = 0x09

get_position_compact = 0x90
get_position_pololu =  0x10

get_moving_state_compact = 0x93
get_moving_state_pololu =   0x13


go_home_compact = 0xA2
go_home_pololu = 0x22

get_errors_compact = 0xA1
get_errors_pololu = 0x21

pololu_cmd = 0xAA
device_number_pololu = 0x0C

control_types = ['ssc', 'compact', 'pololu']
control_modes = ['single', 'multiple']


zeno_head_config = {
    'controllers': {
        'head_controller': {
            'env_port': 'SERIAL_PORT_HEAD',
            'baud_rate': 115200,
            'DOF': 11,
            'default_joint_speed': 15,
        },
    },

    'motorgroups': {
        'neck': ['neck_yaw', 'neck_roll', 'neck_pitch'],
		'eyelids': ['left_eye_yaw', 'right_eye_yaw'],
		'smiles': ['left_smile', 'right_smile'],
		'face': ['left_eye_yaw', 'right_eye_yaw', 'left_smile', 'right_smile', 
                'brows', 'jaw', 'eyelids','eyes_pitch'],
        'head': ['neck_yaw', 'neck_roll', 'neck_pitch', 'left_eye_yaw', 
                 'right_eye_yaw', 'left_smile', 'right_smile',
                 'brows', 'jaw', 'eyelids','eyes_pitch'],
    },

    'motors': {
		'neck_yaw': {
            'id': 0,
            'type': 'AX-12',
            'angle_offset': 46.0,
            'angle_limit': (-16.0, 16.0),
            'pwm_time_offset': 1112,
            'pwm_time_limit': (0.0, 720),

        },
        'neck_roll': {
            'id': 1,
            'type': 'AX-12',
            'angle_offset': 52.0,
            'angle_limit': (-16.0, 8.0),
            'pwm_time_offset': 1304,
            'pwm_time_limit': (0.0, 720),
        },
        'neck_pitch': {
            'id': 2,
            'type': 'AX-12',
            'angle_offset': 44.0,
            'angle_limit': (-16.0, 16.0),
            'pwm_time_offset': 728,
            'pwm_time_limit': (0.0, 1360),
        },
        'brows': {
            'id': 3,
            'type': 'AX-12',
            'angle_offset': 45.0,
            'angle_limit': (-15.0, 15.0),
            'pwm_time_offset': 910,
            'pwm_time_limit': (0.0, 1056),
        },
        'eyelids': {
            'id': 4,
            'type': 'AX-12',
            'angle_offset': 39.0,
            'angle_limit': (-8.0, 7.0),
            'pwm_time_offset': 952,
            'pwm_time_limit': (0.0, 592),
        },
        'eyes_pitch': {
            'id': 5,
            'type': 'AX-12',
            'angle_offset': 60.0,
            'angle_limit': (-3.0, 3.0),
            'pwm_time_offset': 1712,
            'pwm_time_limit': (0.0, 416),
        },
        'jaw': {
            'id': 6,
            'type': 'AX-12',
            'angle_offset': 50.0,
            'angle_limit': (-2.0, 4.0),
            'pwm_time_offset': 1528,
            'pwm_time_limit': (0.0, 144),
        },
        'right_eye_yaw': {
            'id': 8,
            'type': 'AX-12',
            'angle_offset': 38,
            'angle_limit': (-3, 3),
            'pwm_time_offset': 1152,
            'pwm_time_limit': (0.0, 128),
        },
        'left_smile': {
            'id': 9,
            'type': 'AX-12',
            'angle_offset': 48,
            'angle_limit': (-6, 6),
            'pwm_time_offset': 1318,
            'pwm_time_limit': (0.0, 432),
        },
        'left_eye_yaw': {
            'id': 10,
            'type': 'AX-12',
            'angle_offset': 55,
            'angle_limit': (-3, 3),
            'pwm_time_offset': 1695,
            'pwm_time_limit': (0.0, 128),
        },
        'right_smile': {
            'id': 11,
            'type': 'AX-12',
            'angle_offset': 48,
            'angle_limit': (-6, 6),
            'pwm_time_offset': 1415,
            'pwm_time_limit': (0.0, 240),
        },
    },
}

cmake_minimum_required(VERSION 2.8.3)
project(zeno_joint_motion_gui)

find_package(catkin REQUIRED COMPONENTS
  dynamic_reconfigure
  roscpp
  rospy
  trajectory_msgs
  std_msgs
)

generate_dynamic_reconfigure_options(
  cfg/zeno_body_joint_angles_reconfigure_degrees.cfg
  cfg/zeno_head_joint_angles_reconfigure_degrees.cfg
  cfg/zeno_body_joint_angles_reconfigure_radians.cfg
)

catkin_package(
  CATKIN_DEPENDS dynamic_reconfigure roscpp rospy
)

include_directories(
  ${catkin_INCLUDE_DIRS}
)

#==========================================
add_executable(zeno_body_joint_motion_degrees_gui_node 
  src/zeno_body_joint_motion_degrees_gui_node.cpp)

add_dependencies(zeno_body_joint_motion_degrees_gui_node 
  ${PROJECT_NAME}_gencfg)

target_link_libraries(zeno_body_joint_motion_degrees_gui_node
  ${catkin_LIBRARIES}
)

#==================================================
add_executable(zeno_body_joint_motion_radians_gui_node 
  src/zeno_body_joint_motion_radians_gui_node.cpp)

add_dependencies(zeno_body_joint_motion_radians_gui_node 
  ${PROJECT_NAME}_gencfg)

target_link_libraries(zeno_body_joint_motion_radians_gui_node
  ${catkin_LIBRARIES}
)

#==========================================
add_executable(zeno_head_joint_motion_degrees_gui_node 
  src/zeno_head_joint_motion_degrees_gui_node.cpp)

add_dependencies(zeno_head_joint_motion_degrees_gui_node 
  ${PROJECT_NAME}_gencfg)

target_link_libraries(zeno_head_joint_motion_degrees_gui_node
  ${catkin_LIBRARIES}
)

#include <ros/ros.h>
#include <dynamic_reconfigure/server.h>

#include <zeno_joint_motion_gui/zenoJointReconfigureDegreesConfig.h>
#include <trajectory_msgs/JointTrajectory.h>
#include <trajectory_msgs/JointTrajectoryPoint.h>
#include <vector>
#include <string>
#define number_of_joints 21

ros::Publisher pub_;



std::string names[number_of_joints] = {"waist","left_shoulder_pitch", "left_shoulder_roll",
					                    "left_elbow_yaw", "left_elbow_pitch",
					                    "right_shoulder_pitch", "right_shoulder_roll", 
					                    "right_elbow_yaw", "right_elbow_pitch",
					                    "left_hip_roll", "left_hip_yaw", "left_hip_pitch",
					                    "left_knee_pitch", "left_ankle_pitch", "left_ankle_roll",
					                    "right_hip_roll", "right_hip_yaw", "right_hip_pitch",
					                    "right_knee_pitch", "right_ankle_pitch", "right_ankle_roll"};


void dynamicReconfigureBodyDegreesCallback(zeno_joint_motion_gui::zenoJointReconfigureDegreesConfig &config, uint32_t level);

void dynamicReconfigureBodyDegreesCallback(zeno_joint_motion_gui::zenoJointReconfigureDegreesConfig &config, uint32_t level)
{
	trajectory_msgs::JointTrajectory trajectory;
	trajectory_msgs::JointTrajectoryPoint trajectory_points;
	trajectory.points.resize(1);
	std::vector<std::string> joint_names;
	
	int msg_name_index = 0;
	int name_counter=0;
	int resize_counter = 0;
	if(config.Enable_body_joints_cmd)
	{
		if (config.waist_joint) {
			//std::cout << "waist" << std::endl;
			resize_counter++;
			joint_names.resize(resize_counter);
			trajectory_points.positions.resize(resize_counter);

			joint_names.at(msg_name_index) = names[name_counter];
			trajectory_points.positions[0] = config.waist;
			msg_name_index++;
		}
		else {
			name_counter++;
		}

		if (config.left_arm) {
			
			resize_counter += 4;
			joint_names.resize(resize_counter);
			trajectory_points.positions.resize(resize_counter);

			for(int index = msg_name_index; index < msg_name_index+4; index++) {
				joint_names.at(index) = names[name_counter];
				name_counter++;
			}
			//std::cout << "left_arm" << std::endl;
			trajectory_points.positions[msg_name_index] = config.left_shoulder_pitch;
			trajectory_points.positions[msg_name_index+1] = config.left_shoulder_roll;
			trajectory_points.positions[msg_name_index+2] = config.left_elbow_yaw;
			trajectory_points.positions[msg_name_index+3] = config.left_elbow_pitch;

			msg_name_index += 4;

		}else {
			name_counter += 4;
		}

		if (config.right_arm) {
			//std::cout << "right_arm" << std::endl;
			resize_counter += 4;
			joint_names.resize(resize_counter);
			trajectory_points.positions.resize(resize_counter);

			for(int index = msg_name_index; index < msg_name_index+4; index++) {
				joint_names.at(index) = names[name_counter];
				name_counter++;
			}

			trajectory_points.positions[msg_name_index] = config.right_shoulder_pitch;
			trajectory_points.positions[msg_name_index+1] = config.right_shoulder_roll;
			trajectory_points.positions[msg_name_index+2] = config.right_elbow_yaw;
			trajectory_points.positions[msg_name_index+3] = config.right_elbow_pitch;

			msg_name_index += 4;


		}else {
			name_counter += 4;
		}

		if (config.left_leg) {
			//std::cout << "right_arm" << std::endl;
			resize_counter += 6;
			joint_names.resize(resize_counter);
			trajectory_points.positions.resize(resize_counter);

			for(int index = msg_name_index; index < msg_name_index+6; index++) {
				joint_names.at(index) = names[name_counter];
				name_counter++;
			}

			trajectory_points.positions[msg_name_index] = config.left_hip_roll;
			trajectory_points.positions[msg_name_index+1] = config.left_hip_yaw;
			trajectory_points.positions[msg_name_index+2] = config.left_hip_pitch;
			trajectory_points.positions[msg_name_index+3] = config.left_knee_pitch;
			trajectory_points.positions[msg_name_index+4] = config.left_ankle_pitch;
			trajectory_points.positions[msg_name_index+5] = config.left_ankle_roll;

			msg_name_index += 6;


		}else {
			name_counter += 6;
		}

		if (config.right_leg) {
			//std::cout << "right_arm" << std::endl;
			resize_counter += 6;
			joint_names.resize(resize_counter);
			trajectory_points.positions.resize(resize_counter);

			for(int index = msg_name_index; index < msg_name_index+6; index++) {
				joint_names.at(index) = names[name_counter];
				name_counter++;
			}

			trajectory_points.positions[msg_name_index] = config.right_hip_roll;
			trajectory_points.positions[msg_name_index+1] = config.right_hip_yaw;
			trajectory_points.positions[msg_name_index+2] = config.right_hip_pitch;
			trajectory_points.positions[msg_name_index+3] = config.right_knee_pitch;
			trajectory_points.positions[msg_name_index+4] = config.right_ankle_pitch;
			trajectory_points.positions[msg_name_index+5] = config.right_ankle_roll;
		}

		trajectory.joint_names = joint_names;

		trajectory.points[0] = trajectory_points;
		if(sizeof(joint_names) != 0)
			pub_.publish(trajectory);
	}
}

int main (int argc, char** argv)
{
	// Initialize ROS
	ros::init (argc, argv, "zeno_joint_motion_degrees_gui_node");
	ros::NodeHandle nh("~");

	pub_ = nh.advertise<trajectory_msgs::JointTrajectory>("/body_controller/trajectory_command", 100);

	dynamic_reconfigure::Server<zeno_joint_motion_gui::zenoJointReconfigureDegreesConfig> server_degrees;
	dynamic_reconfigure::Server<zeno_joint_motion_gui::zenoJointReconfigureDegreesConfig>::CallbackType f_degrees;
	f_degrees = boost::bind(&dynamicReconfigureBodyDegreesCallback, _1, _2);
	server_degrees.setCallback(f_degrees);

	ROS_INFO("Body Joints GUI node is running. Use rqt_reconfigure to test joints.");
	ros::spin();
}

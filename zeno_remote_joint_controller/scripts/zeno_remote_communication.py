#!/usr/bin/python
import socket
import time
from json import dumps
from remote_server_config import *

class ZenoRemoteCommunication():
    def __init__(self, remote_host, remote_host_port):
        self.remote_host_address = remote_addr
        self.remote_host_port = remote_port
        self.connection_timeout = 1.0
        #self.initilize_socket_connection()

    def initialize_connection(self):
        #connect to the socket
        try: 
            self.host_socket_interface = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.host_socket_interface.settimeout(self.connection_timeout)
            self.host_socket_interface.connect((self.remote_host_address, self.remote_host_port))
        
        except:
            return False    

        return True
    
    def register_topic(self, operation, topic_name, topic_type):
        register_message = {
                             "op": operation, 
                             "topic": topic_name, 
                             "type": topic_type
                           }

        register_message_json = dumps(register_message)
        self.host_socket_interface.send(str(register_message_json))

    def publish_message(self, topic_name, message):
        pub_message = {
                        "op": "publish", 
                        "topic": topic_name, 
                        "msg": message
                      }

        pub_message_json = dumps(pub_message)
        self.host_socket_interface.send(str(pub_message_json))

    def get_subscribe_message(self):
        try:
            sub_msg = self.host_socket_interface.recv(1024)
            return sub_msg
        except:
            return None

    def shutdown(self):
        self.host_socket_interface.close()

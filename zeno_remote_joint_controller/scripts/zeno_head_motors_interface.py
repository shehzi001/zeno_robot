#!/usr/bin/python
import serial
import sys
import time
from zeno_head_configuration import *
from zeno_head_configuration import zeno_head_config as zhc

class ZenoHeadMotorsInterface():
    def __init__(self, port_name, port_baud_rate):
        self.serial_port_name = port_name
        self.serial_baud_rate = port_baud_rate
        self.is_controller_initialized = False

    def initilize_head_interface(self):
        #print 'Info: Initilizing Serial Connection using port ' + self.serial_port_name
        try:
            self.head_serial_interface = serial.Serial(self.serial_port_name)
            self.head_serial_interface.baudrate = self.serial_baud_rate
        except:
            #print 'Error: Serial port initilization failed.'
            return False

        self.is_controller_initialized = True
        return True

    def go_home(self):
        ins = chr(go_home_compact)
        return self.write_instruction(ins)

    def set_motor_position_command(self, number_of_motors, motor_id, motor_angle):

        motor_angle = int(motor_angle)
        #Get the lowest 7 bits
        byteone = motor_angle&127
        #Get the highest 7 bits
        bytetwo = (motor_angle-(motor_angle&127))/128

        motor_command = chr(bytetwo) + chr(byteone)
        
        ins = chr(set_position_compact_multiple) + chr(number_of_motors) + chr(motor_id) + motor_command
        return self.write_instruction(ins)

    def set_motor_velocity_command(self, motor_id, motor_speed):
        
        motor_speed = int(motor_speed)
        byteone = motor_speed & 127
        #Get the highest 7 bits
        bytetwo = (motor_speed-(motor_speed & 127))/128
        ins = chr(set_speed_compact)+ chr(motor_id) + chr(byteone) + chr(bytetwo)
        return self.write_instruction(ins)

    def write_instruction(self, instruction):
        try:
            self.head_serial_interface.write(instruction)
            time.sleep(0.1)
            return True
        except:
            print 'Error: Serial write failed.'
            return False

    def get_pwm_time(self, motor_id):
        ins = chr(get_position_compact) + chr(motor_id)
        try:
            self.head_serial_interface.write(ins)
            lb = self.head_serial_interface.read()
            hb = self.head_serial_interface.read()
            pwm_hex = hb + lb
            pwm_time = int(pwm_hex.encode("hex"),16)/4

            return pwm_time
        except:
            print 'Error: Serial read failed.'
            return 0.0
            #position = (pwm_time - 1104)*(32.0/720)

    def shutdown(self):
        if(self.is_controller_initialized):
            self.head_serial_interface.close()

    def __del__(self):
        self.shutdown()
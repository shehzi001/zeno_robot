#!/usr/bin/python
from zeno_body_config import *
import pypot.robot
import time 
import pypot.dynamixel

import json

class ZenobodyController():
    def __init__(self):
        self.is_controller_initialized =  False 

    def initialization(self):
        try:
            self.robot = pypot.robot.from_config(zeno_robot_config)
            self.robot.start_sync()
            time.sleep(1.0)
        except ValueError:
            return False

        self.is_controller_initialized = True
        return True

    def initialize_joints(self):
        self.position_cmd_execution_time = 0.75
        self.joint_names =  zeno_robot_config.get('motorgroups').get('robot')
        joint_positions = [0.0] * len(self.joint_names)
        self.move_to(self.joint_names, joint_positions)
        return True

    def get_joint_states(self):
        joint_states = []
        joint_position = []
        joint_velocity = []
        joint_effort = []
        for m in self.robot.robot:
            joint_position.append(m.present_position)
            joint_velocity.append(m.present_speed)
            joint_effort.append(m.present_load)

        msg_fields = ["name", "position", "velocity", "effort"]
        msg_bundle = [self.joint_names, 
                      joint_position, 
                      joint_velocity,
                      joint_effort]
        msg = dict(zip(msg_fields, msg_bundle))

        return msg
    
    def move_to_speed_position(self, joint_names, joints_positions, joint_speeds):
        for m in self.robot.motors:
           motor_name = m.name
           if motor_name in joint_names:
               index = joint_names.index(motor_name)
               m.goal_speed = joint_speeds[index]
               m.goal_position = joints_positions[index]

    def move_to(self, joint_names, joint_angle):
        #print "joint_angle", joint_angle
        position_command = dict(zip(joint_names, joint_angle))
        self.robot.goto_position(position_command, self.position_cmd_execution_time, False)

    def shutdown(self):
        if(self.is_controller_initialized):
            self.robot.stop_sync()
            self.robot.close()

    def __del__(self):
        self.shutdown()

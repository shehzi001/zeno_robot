#!/usr/bin/python
from remote_server_config import *

import time 
from zeno_remote_communication import *
from zeno_body_controller import *
from zeno_speech_server_interface import *
from zeno_head_controller import *

import json

import signal
import sys

ACTIVE=True

def signal_handler(signal, frame):
        print('You pressed Ctrl+C!')
        ACTIVE=False
        #sys.exit(0)

signal.signal(signal.SIGINT, signal_handler)

class ZenoSpeechController():
    def __init__(self):
        self.selected_controller = 'speech_controller'

        self.is_remote_connection_active = False
        self.is_speech_interface_active = False

        self.initization()

        self.initilization_msgs()

        #self.initialize_joints()

        while ACTIVE:
            try:
                #time.sleep(0.01)
                self.execute_cycle()
            except:
                self.shutdown()
                break

    def execute_cycle(self):
        self.initization()

        #if(self.is_remote_connection_active):
        #    self.is_remote_connection_active = self.publish_joint_states()

        if(self.is_remote_connection_active):
            self.is_remote_connection_active = self.get_subscribe_topics_msgs()

    def initization(self):
        if(not(self.is_remote_connection_active)):
            self.is_remote_connection_active = self.initialize_remote_communication()

        #if(not(self.is_body_controller_active)):
        #    self.is_body_controller_active = self.initialize_body_controller()

        #if(not(self.is_head_controller_active)):
        #    self.is_head_controller_active = self.initialize_head_controller()

        if(not(self.is_speech_interface_active)):
            self.is_speech_interface_active = self.initialize_speech_server_interface()

    def initilization_msgs(self):
        success = True
        if(not(self.is_remote_connection_active)):
            print "Failed to initialize remote communication interface."
            success = False
        #if(not(self.is_body_controller_active)):
        #    print "Failed to initialize body controller."
        #    success = False
        #if(not(self.is_head_controller_active)):
        #    print "Failed to initialize head controller."
        #    success = False
        if(not(self.is_speech_interface_active)):
            print "Failed to initialize speech interface."
            success = False

        if(not(success)):
            print "Controller will keep trying to initialize."

    #def initialize_body_controller(self):
    #    self.body_controller = ZenobodyController()
    #    success = self.body_controller.initialization()
    #    if (not(success)):
    #        return success#
#
#        print "Body controller is initialized...." #
#
#        return success

#    def initialize_head_controller(self):
#        self.head_controller = ZenoHeadController()
#        success  = self.head_controller.initialization()

#        if (not(success)):
#            return success

#       print "Head controller is initialized...." 

#        return success

    def initialize_speech_server_interface(self):    
        self.speech_server_interface = ZenoSpeechServerInterface()
        success = self.speech_server_interface.initialization()

        if (not(success)):
            return success

        print "Speech Server Interface is initialized...." 

        return success

 #   def initialize_joints(self):
#
#        if(self.is_head_controller_active):
#            self.initialize_head_joints()

#        if(self.is_body_controller_active):
#            self.initialize_body_joints()

#    def initialize_head_joints(self):
#        success = self.head_controller.initialize_joints()
#        if(not(success)):
#            print "Failed to initialize head joints."
#            return success

#        return success

#    def initialize_body_joints(self):

#        success = self.body_controller.initialize_joints()
#        if(not(success)):
#            print "Failed to initialize body joints."
#            return success

#        return success

    def initialize_remote_communication(self):
        #print "Initializing Remote Communication....."       
        self.remote_communication = ZenoRemoteCommunication(remote_addr, remote_port)
        self.is_remote_connection_active = self.remote_communication.initialize_connection()

        if (not(self.is_remote_connection_active)):
            #print "Failed to connect to remote server."
            #print "Joint controller will keep trying to connect."
            return False

        self.register_remote_messages()

        print "Remote Communication interface is initialized...." 

        return True
        time.sleep(0.1)

    def register_remote_messages(self):
 #       self.remote_communication.register_topic("advertise", 
 #                   "joint_states", "sensor_msgs/JointState")
 #       self.remote_communication.register_topic("subscribe", 
 #                   "body_position_command", "zeno_command_msgs/PositionCommand")
 #       self.remote_communication.register_topic("subscribe", 
 #                   "body_joints_command", "zeno_command_msgs/JointsCommand")
       self.remote_communication.register_topic("subscribe", 
                   "speech_command", "std_msgs/String")
 #       self.remote_communication.register_topic("subscribe", 
 #                   "head_position_command", 
 #                   "zeno_command_msgs/PositionCommand")

#    def publish_joint_states(self):
#        states_msg = None
#
#        try:
#            if ((self.selected_controller == 'head_controller') and 
# #               (self.is_head_controller_active)):
#                states_msg = self.head_controller.get_joint_states()
#
#            elif (self.selected_controller == 'body_controller'and 
#                (self.is_body_controller_active)):
#                states_msg = self.body_controller.get_joint_states()
#        except:
#            pass#
#
#        if((self.selected_controller == 'head_controller')):
#            self.selected_controller = 'body_controller'
#        elif((self.selected_controller == 'body_controller')):
#            self.selected_controller = 'head_controller'
#
#        topic_name = "joint_states"#
#
#        try:
#            if(not(states_msg == None)):
#                self.remote_communication.publish_message(topic_name, states_msg)
#        except:
#            return False
#
#        return True

    def get_subscribe_topics_msgs(self):
        try:
            recv_msg = self.remote_communication.get_subscribe_message()
        except:
            return False

        if not(recv_msg == None):
            try:
                d = json.loads(recv_msg)
                topic_name = d.get("topic")
                if topic_name == "speech_command":
                    msg = d.get("msg")
                    speech_msg = msg.get("data")
                    if(self.is_speech_interface_active):
                        self.speech_server_interface.write_speech_text(speech_msg)
#
#                elif topic_name == "head_position_command":
#                    msg = d.get("msg")
#                    joint_names = msg.get("joints_names")
#                    joint_angles = msg.get("joint_positions")
#                    if(self.is_head_controller_active):
#                        self.head_controller.set_joint_position_commands(joint_names, joint_angles)
            except ValueError:
                pass

        return True

    def shutdown(self):
        print "Shutting down joint controller.."
 #       if(self.is_body_controller_active):
 #           self.body_controller.shutdown()
        if(self.is_remote_connection_active):
            self.remote_communication.shutdown()
 #       if(self.is_head_controller_active):
 #           self.head_controller.shutdown()

    def __del__(self):
        self.shutdown()

if __name__ == '__main__':
    zeno_speech_controller = ZenoSpeechController()

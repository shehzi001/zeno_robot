#!/usr/bin/python
# -*- coding: iso-8859-1 -*-

import codecs

from zeno_speech_config import *

class ZenoSpeechServerInterface():
    def __init__(self):
        self.speech_file = speech_file

    def initialization(self):
        return True

    def write_speech_text(self, speech_text):
        file_handle = codecs.open(self.speech_file, 'w','utf-8')
        file_handle.write(speech_text)
        file_handle.close()

    def shutdown(self):
        pass
        #self.file_handle.close()

    def __del__(self):
        self.shutdown()
        
        
#!/usr/bin/python
import serial
import sys
import time
#set up the serial port for action
#ser=serial.Serial(0)
#ser.baudrate=2400
#set up the serial port for action (0==COM1==ttyS0)
ser=serial.Serial("/dev/ttyACM0")
ser.baudrate=115200

def setspeed(n,speed):
  #Quick check that things are in range
  if speed > 127 or speed <0:
    speed=1
    print "WARNING: Speed should be between 0 and 127. Setting speed to 1..."
    print "Setting servo "+str(n)+" speed to "+str(speed)+" out of 127."
  speed=int(speed)
  #set speed (needs 0x80 as first byte, 0x01 as the second, 0x01 is for speed, 0 for servo 0, and 127 for max speed)
  bud=chr(0x80)+chr(0x01)+chr(0x01)+chr(n)+chr(speed)
  ser.write(bud)

def setpos(n,angle):
  #Check that things are in range
  if angle > 180 or angle <0:
    angle=90
    print "WARNING: Angle range should be between 0 and 180. Setting angle to 90 degrees to be safe..."
    print "moving servo "+str(n)+" to "+str(angle)+" degrees."

  #Valid range is 500-5500
  #offyougo=int(720*angle/180)+1104
  offyougo = angle
  print offyougo
  #Get the lowest 7 bits
  byteone=offyougo&127
  print byteone
  #Get the highest 7 bits
  bytetwo=(offyougo-(offyougo&127))/128
  print bytetwo
  #move to an absolute position in 8-bit mode (0x04 for the mode, 0 for the servo, 0-255 for the position (spread over two bytes))
  #bud=chr(0x80)+chr(0x01)+chr(0x04)+chr(n)+chr(bytetwo)+chr(byteone)
  #bud = chr(0x9F)+ chr(0x02)+ chr(0x00) + chr(0x50)+ chr(0x08) + chr(0x10)+ chr(0x0A)
  bud = chr(0x87)+ chr(0x00)+ chr(0x08) + chr(0x00)
  ser.write(bud)
  time.sleep(2.0)
  bud = chr(0x9F) + chr(0x01)+ chr(n) + chr(bytetwo) + chr(byteone) 
  #bud = chr(0x9F) + chr(0x02)+ chr(0x00) + chr(0x20)+ chr(0x07) + chr(0x78)+ chr(0x0C)  #for 0 chr(0x50)+ chr(0x08)#
  ser.write(bud)
  #time.sleep(2.0)
  ser.close()
  time.sleep(2.0)

def get_position(n):
  ins = chr(0x90) + chr(n)

  ser.write(ins)
  lb = ser.read()
  hb = ser.read()
  print 'lower_byte:', int(lb.encode("hex"),16)
  print 'higher_byte:', int(hb.encode("hex"),16)
  byte =    hb + lb
  pwm_time = int(byte.encode("hex"),16)/4

  #shift_time = (pwm_time - 1104)*(32.0/720)

  print pwm_time

def go_home():
  time.sleep(2.0)
  bud = chr(0xA2)
  ser.write(bud)      
  time.sleep(2.0)
  ser.close()
  time.sleep(1.0)

mode=sys.argv[1]
n=int(sys.argv[2])
m=int(sys.argv[3])
if mode=='speed':
  print "speed control"
  setspeed(n,m)
elif mode=='pos':
  print "pos control"
  setpos(n,m)
elif mode=='home':
  print "moving joints to home position"
  go_home()
elif mode=='rpos':
  get_position(n)
else:
  print "No commands given.\nUsage: controlfunction.py speed <servo> <speed>, or\n control"

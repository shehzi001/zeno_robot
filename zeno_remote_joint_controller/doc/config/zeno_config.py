#!/usr/bin/python

remote_host = "192.168.188.25"
remote_host_port = 9090

zeno_robot_config = {
    'controllers': {
        'upper_body_controller': {
            'port': '/dev/ttyUSB3', # Depends on your OS
            'sync_read': False,
            'attached_motors': ['base', 'left_arm', 'right_arm', 'left_leg', 'right_leg'],
        },
    },

    'motorgroups': {
        'base': ['waist'],
        'left_arm': ['left_shoulder_pitch', 'left_shoulder_roll', 'left_elbow_yaw', 'left_elbow_pitch'],
        'right_arm': ['right_shoulder_pitch', 'right_shoulder_roll', 'right_elbow_yaw', 'right_elbow_pitch'],
        'left_leg': ['left_hip_roll', 'left_hip_yaw', 'left_hip_pitch', 'left_knee_pitch', 'left_ankle_pitch', 'left_ankle_roll'],
        'right_leg': ['right_hip_roll', 'right_hip_yaw', 'right_hip_pitch', 'right_knee_pitch', 'right_ankle_pitch', 'right_ankle_roll'],
    },

    'motors': {
        'waist': {
            'id': 0,
            'type': 'RX-28',
            'orientation': 'direct',
            'offset': 0.0,
            'angle_limit': (-59.38, 67.3),
        },
        'left_shoulder_pitch': {
            'id': 1,
            'type': 'RX-28',
            'orientation': 'direct',
            'offset': 90.47,
            'angle_limit': (-118.62, 144.43),
        },
        'left_shoulder_roll': {
            'id': 2,
            'type': 'RX-28',
            'orientation': 'direct',
            'offset': -82.55,
            'angle_limit': (-81.38, 7.48),
        },
        'left_elbow_yaw': {
            'id': 3,
            'type': 'RX-28',
            'orientation': 'direct',
            'offset': 0.0,
            'angle_limit': (-90.76, 90.47),
        },
        'left_elbow_pitch': {
            'id': 4,
            'type': 'RX-28',
            'orientation': 'direct',
            'offset': 7.48,
            'angle_limit': (6.6, 85.48),
        },
        'right_shoulder_pitch': {
            'id': 5,
            'type': 'RX-28',
            'orientation': 'direct',
            'offset': -88.71,
            'angle_limit': (-144.43, 112.46),
        },
        'right_shoulder_roll': {
            'id': 6,
            'type': 'RX-28',
            'orientation': 'direct',
            'offset': 80.79,
            'angle_limit': (-6.89, 87.54),
        },
        'right_elbow_yaw': {
            'id': 7,
            'type': 'RX-28',
            'orientation': 'direct',
            'offset': -91.06,
            'angle_limit': (-99.56, 98.39),
        },
        'right_elbow_pitch': {
            'id': 8,
            'type': 'RX-28',
            'orientation': 'direct',
            'offset': 0.0,
            'angle_limit': (0.15, 91.94),
        },
        'left_hip_roll': {
            'id': 9,
            'type': 'RX-64',
            'orientation': 'direct',
            'offset': 21.26,
            'angle_limit': (-2.49, 96.04),
        },
        'left_hip_yaw': {
            'id': 10,
            'type': 'RX-28',
            'orientation': 'direct',
            'offset': 3.96,
            'angle_limit': (-102.49, 65.54),
        },
        'left_hip_pitch': {
            'id': 11,
            'type': 'RX-64',
            'orientation': 'direct',
            'offset': 0.0,
            'angle_limit': (-3.08, 90.18),
        },
        'left_knee_pitch': {
            'id': 12,
            'type': 'RX-64',
            'orientation': 'direct',
            'offset': 0.0,
            'angle_limit': (-13.34, 93.11),
        },
        'left_ankle_pitch': {
            'id': 13,
            'type': 'RX-64',
            'orientation': 'direct',
            'offset': 0.0,
            'angle_limit': (-28.89, 63.49),
        },
        'left_ankle_roll': {
            'id': 14,
            'type': 'RX-64',
            'orientation': 'direct',
            'offset': 0.0,
            'angle_limit': (-31.23, 31.52),
        },
        'right_hip_roll': {
            'id': 15,
            'type': 'RX-64',
            'orientation': 'direct',
            'offset': 19.5,
            'angle_limit': (-48.53, 24.78),
        },
        'right_hip_yaw': {
            'id': 16,
            'type': 'RX-28',
            'orientation': 'direct',
            'offset': 6.0,
            'angle_limit': (-71.11, 101.32),
        },
        'right_hip_pitch': {
            'id': 17,
            'type': 'RX-64',
            'orientation': 'direct',
            'offset': 0.0,
            'angle_limit': (-15.4, 90.76),
        },
        'right_knee_pitch': {
            'id': 18,
            'type': 'RX-64',
            'orientation': 'direct',
            'offset': 0.0,
            'angle_limit': (-2.49, 89.88),
        },
        'right_ankle_pitch': {
            'id': 19,
            'type': 'RX-64',
            'orientation': 'direct',
            'offset': 0.0,
            'angle_limit': (-31.52, 72.58),
        },
        'right_ankle_roll': {
            'id': 20,
            'type': 'RX-64',
            'orientation': 'direct',
            'offset': 0.0,
            'angle_limit': (-32.99, 27.71),
        },
    },
}

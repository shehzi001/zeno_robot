#!/usr/bin/python

remote_addr = "192.168.188.25"
remote_port = 9090
speech_file = "/tmp/speech_pipe"

zeno_robot_config = {
    'controllers': {
        'upper_body_controller': {
            'port': '/dev/ttyUSB3', # Depends on your OS
            'sync_read': False,
            'attached_motors': ['robot', 'left_arm', 'right_arm'],
        },
    },

    'motorgroups': {
        'robot': ['left_shoulder_pitch', 'left_shoulder_roll', 'left_elbow_yaw', 'left_elbow_pitch',
                  'right_shoulder_pitch', 'right_shoulder_roll', 'right_elbow_yaw', 'right_elbow_pitch'],
		'left_arm': ['left_shoulder_pitch', 'left_shoulder_roll', 'left_elbow_yaw', 'left_elbow_pitch'],
		'right_arm': ['right_shoulder_pitch', 'right_shoulder_roll', 'right_elbow_yaw', 'right_elbow_pitch'],
    },

    'motors': {
		'left_shoulder_pitch': {
            'id': 1,
            'type': 'RX-28',
            'orientation': 'indirect',
            'offset': -82,
            'angle_limit': (-118, 144),
        },
        'left_shoulder_roll': {
            'id': 2,
            'type': 'RX-28',
            'orientation': 'direct',
            'offset': -78,
            'angle_limit': (-81.0, 7.0),
        },
        'left_elbow_yaw': {
            'id': 3,
            'type': 'RX-28',
            'orientation': 'indirect',
            'offset': -90.0,
            'angle_limit': (-90.0, 90.0),
        },
        'left_elbow_pitch': {
            'id': 4,
            'type': 'RX-28',
            'orientation': 'direct',
            'offset': 8.0,
            'angle_limit': (8.0, 89.0),
        },
        'right_shoulder_pitch': {
            'id': 5,
            'type': 'RX-28',
            'orientation': 'direct',
            'offset': -82,
            'angle_limit': (-144, 112),
        },
        'right_shoulder_roll': {
            'id': 6,
            'type': 'RX-28',
            'orientation': 'indirect',
            'offset': -78,
            'angle_limit': (-6.0, 87.0),
        },
        'right_elbow_yaw': {
            'id': 7,
            'type': 'RX-28',
            'orientation': 'direct',
            'offset': -90.0,
            'angle_limit': (-90.0, 90.0),
        },
        'right_elbow_pitch': {
            'id': 8,
            'type': 'RX-28',
            'orientation': 'direct',
            'offset': 8.0,
            'angle_limit': (8.0, 89.0),
        },
    },
}
